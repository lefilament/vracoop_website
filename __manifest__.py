{
    "name": "VRACOOP - Customisation Website Sale",
    "summary": "VRACOOP - Customisation Website Sale",
    "version": "12.0.1.0.1",
    "development_status": "Beta",
    "author": "Le Filament",
    "maintainers": ["remi-filament"],
    "license": "AGPL-3",
    "application": False,
    "installable": True,
    "depends": [
        "website", "website_sale", "vracoop_click_and_collect"
    ],
    "data": [
        "views/assets.xml",
        "views/templates.xml",
    ]
}
