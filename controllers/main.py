# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import http, _
from odoo.http import request
from odoo.addons.website_sale.controllers.main import WebsiteSale
from odoo.addons.website_sale_delivery.controllers.main import WebsiteSaleDelivery


class VracoopWebsiteSale(WebsiteSale):

    def _get_mandatory_billing_fields(self):
        res = super(VracoopWebsiteSale, self)._get_mandatory_billing_fields()
        return ["name", "email", "phone"]

    def _get_mandatory_shipping_fields(self):
        res = super(VracoopWebsiteSale, self)._get_mandatory_shipping_fields()
        return ["name"]
