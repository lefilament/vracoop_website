# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import fields, models


class VracoopDeliveryCarrier(models.Model):
    _inherit = 'delivery.carrier'

    type_carrier = fields.Selection(selection_add=[('livraison', 'Livraison')])
