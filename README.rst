.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl
   :alt: License: AGPL-3


====================================
VRACOOP - Customisation Website Sale
====================================

Modifie le module Website Sale:
    - Modification du symbole Ajout panier
    - Modification du libellé bouton pour passer au paiement
    - Modification du formulaire Adresse

Credits
=======

Contributors
------------

* Juliana Poudou <juliana@le-filament.com>

Maintainer
----------

.. image:: https://le-filament.com/images/logo-lefilament.png
   :alt: Le Filament
   :target: https://le-filament.com

This module is maintained by Le Filament
